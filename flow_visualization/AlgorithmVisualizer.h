#ifndef ALGORITHM_VISUALIZER_H_
#define ALGORITHM_VISUALIZER_H_

#include <unordered_map>
#include <vector>
#include <string>
#include <QPainter>
#include <QWidget>
#include <sstream>

#include "Flow/AlgorithmDescription.h"
#include "Flow/FlowNetwork.h"
#include "FlowNetworkPainter.h"

typedef unsigned long long excess_t;


class AlgorithmVisualizer : public QObject {
    Q_OBJECT

public:
    virtual void draw(QPainter* painter) = 0;

    size_t getCurrentStepIndex() {
        return currentStepIndex;
    }

    virtual ~AlgorithmVisualizer() {}

signals:
    void stepIndexChanged(int newIndex);
    void imageChanged();

public slots:
    void nextStep() {
        if (currentStepIndex < steps.size()) {
            steps[currentStepIndex].makeStep();
            currentStepIndex++;
        }
        repaint();
        emit stepIndexChanged(currentStepIndex);
        emit imageChanged();
    }

    void previousStep() {
        if (currentStepIndex > 0) {
            currentStepIndex--;
            steps[currentStepIndex].cancelStep();
        }
        repaint();
        emit stepIndexChanged(currentStepIndex);
        emit imageChanged();
    }

    virtual void repaint() = 0;

protected:
    size_t currentStepIndex;
    std::vector<Step> steps;

    std::string getLastStepDescription() {
        if (currentStepIndex > 0)
            return steps[currentStepIndex - 1].description;
        else
            return "";
    }
};

class DischargeEverythingVisualizer : public AlgorithmVisualizer {
    Q_OBJECT

public:
    DischargeEverythingVisualizer(QWidget* parent, const FlowNetwork& flowNet);
    ~DischargeEverythingVisualizer();

    virtual void draw(QPainter* painter);

public slots:
    void repaint();

private:
    /// Поля необходимые для визуализации
    FlowNetwork net;
    std::vector<excess_t> nodeExcesses;
    std::vector<size_t> nodeHeights;
    std::vector<size_t> nodeColors;
    std::unordered_map<Arc, size_t> arcColors;

    /// Рисовальщики
    FlowNetworkPainter* netPainter;
    TextPainter* textPainter;
    NodeQueuePainter* queuePainter;

    /// Виджеты
    QWidget* parentWidget;
    QWidget* mainWidget;
    QWidget* descriptionWidget;
    QWidget* queueWidget;

    /// Поля и методы для построения описания
    /// Для каждого узла хранит указатель на указатель на текущее ребро списка смежности
    std::vector< std::list<ArcPtr>::const_iterator > currentArcPtr;
    void initializeDescription();
    void makeDescription();
    void addStep(Step& step);
    void addDescription(Step& step, std::string description);
    void setNodeColor(Step& step, size_t nodeIndex, size_t color);
    void setArcColor(Step& step, Arc arc, size_t color);
    void addFlow(Step& step, ArcPtr arcPtr, int flow);
    void addExcess(Step& step, size_t nodeIndex, int flow);
    void setHeight(Step& step, size_t nodeIndex, size_t height);
    bool canDischarge(Step& step, size_t nodeIndex);
    void discharge(Step& step, size_t nodeIndex);
    bool canPush(Step& step, ArcPtr arcPtr);
    void push(Step& step, ArcPtr arcPtr);
    void relabel(Step& step, size_t nodeIndex);
};

#endif // ALGORITHM_VISUALIZER_H_
