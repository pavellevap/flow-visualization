#ifndef MAIN_WINDOW_H_
#define MAIN_WINDOW_H_

#include <QMainWindow>
#include <QPaintEvent>
#include <QPainter>

#include "FlowNetworkPainter.h"
#include "Flow/MaxFlowAlgorithm.h"
#include "Flow/RandFlowNet.h"
#include "AlgorithmVisualizer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void generateFlowNetwork();

private:
    Ui::MainWindow *ui;

    AlgorithmVisualizer *visualizer;

    void paintEvent(QPaintEvent *event);
};

#endif // MAIN_WINDOW_H_
