#-------------------------------------------------
#
# Project created by QtCreator 2015-09-30T11:16:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = flow_visualization
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Flow/RandFlowNet.cpp \
    Flow/MaxFlowAlgorithm.cpp \
    Flow/FlowNetwork.cpp \
    NodeWidget.cpp \
    FlowNetworkPainter.cpp \
    AlgorithmVisualizer.cpp

HEADERS  += mainwindow.h \
    Flow/RandFlowNet.h \
    Flow/MaxFlowAlgorithm.h \
    Flow/FlowNetwork.h \
    NodeWidget.h \
    FlowNetworkPainter.h \
    Flow/AlgorithmDescription.h \
    AlgorithmVisualizer.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11
QMAKE_LFLAGS   += -std=c++11
