#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    visualizer = NULL;
    generateFlowNetwork();
    connect(ui->generateNetButton, SIGNAL(released()), this, SLOT(generateFlowNetwork()));
}

MainWindow::~MainWindow() {
    delete ui;
    if (visualizer != NULL) {
        delete visualizer;
        visualizer = NULL;
    }
}

void MainWindow::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    if (visualizer != NULL)
        visualizer->draw(&painter);
}

void MainWindow::generateFlowNetwork() {
    FlowNetwork flowNetwork;
    createRandNet(flowNetwork, ui->seedSpinBox->value(), ui->amountOfNodesSpinBox->value(),
                  ui->densitySpinBox->value(), 100);

    if (visualizer != NULL)
        delete visualizer;
    visualizer = new DischargeEverythingVisualizer(ui->frame, flowNetwork);
    connect(ui->forwardButton, SIGNAL(released()), visualizer, SLOT(nextStep()));
    connect(ui->backButton, SIGNAL(released()), visualizer, SLOT(previousStep()));
    connect(visualizer, SIGNAL(imageChanged()), this, SLOT(repaint()));
    connect(visualizer, SIGNAL(stepIndexChanged(int)), ui->currentStepSpinBox, SLOT(setValue(int)));

    repaint();
}
