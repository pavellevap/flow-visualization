#ifndef ALGORITHM_DESCRIPTION_H_
#define ALGORITHM_DESCRIPTION_H_

#include <vector>
#include <list>
#include <string>
#include <map>
#include <iostream>

#include "FlowNetwork.h"

/*
 * Описание алгоритма (AlgorithmDescription) состоит из последовательности шагов (Step).
 * Каждый шаг можно как  выполнить (Step::makeStep) так и отменить (Step::cancelStep)
 * Также каждый шаг имеет описание (Step::description).
 *
 * Шаг состоит из последовательности действий (Action)
 * Каждое действие можно как выполнить (Action::makeAction), так и отменить (Action::cancelAction)
 * Каждое действие изменяет состояние алгоритма (AlgorithmDescription::algorithmState)
 */

class Action {
public:
    virtual void makeAction() = 0;
    virtual void cancelAction() = 0;
};

class Step {
public:
    void makeStep() {
        for (auto it = actions.begin(); it != actions.end(); it++)
            (*it)->makeAction();
    }

    void cancelStep() {
        for (auto it = actions.rbegin(); it != actions.rend(); it++)
            (*it)->cancelAction();
    }

    std::string description;
    std::vector<Action*> actions;
};

template <class StateType>
class AlgorithmDescription {
public:
    void nextStep() {
        if (currentStepIndex < steps.size()) {
            steps[currentStepIndex].makeStep();
            currentStepIndex++;
        }
    }

    void previousStep() {
        if (currentStepIndex > 0) {
            currentStepIndex--;
            steps[currentStepIndex].cancelStep();
        }
    }

    std::string getLastStepDescription() const {
        if (currentStepIndex > 0)
            return steps[currentStepIndex - 1].description;
    }
    StateType getCurrentState() const {
        return algorithmState;
    }

    size_t currentStepIndex;
    std::vector<Step> steps;
    StateType algorithmState;
};

template <class VariableType>
class SetVariableAction : public Action {
public:
    SetVariableAction(VariableType* _varPtr, VariableType _newValue) :
        varPtr(_varPtr), oldValue(*_varPtr), newValue(_newValue) {}

    void makeAction() {
        *varPtr = newValue;
    }

    void cancelAction() {
        *varPtr = oldValue;
    }

private:
    VariableType* varPtr;
    VariableType oldValue, newValue;
};

template <class VariableType>
class AddToVariableAction : public Action {
public:
    AddToVariableAction(VariableType* _varPtr, VariableType _delta) :
        varPtr(_varPtr), delta(_delta) {}

    void makeAction() {
        *varPtr += delta;
    }

    void cancelAction() {
        *varPtr -= delta;
    }

private:
    VariableType* varPtr;
    VariableType delta;
};

class AddFlowAction : public Action {
public:
    AddFlowAction(ArcPtr _arcPtr, int _flow) :
        arcPtr(_arcPtr), flow(_flow) {}

    void makeAction() {
        arcPtr->addFlow(flow);
    }

    void cancelAction() {
        arcPtr->addFlow(-flow);
    }

private:
    ArcPtr arcPtr;
    int flow;
};



#endif // ALGORITHM_DESCRIPTION_H_
