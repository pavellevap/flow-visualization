#ifndef RAND_FLOW_NET_H_
#define RAND_FLOW_NET_H_

#include "FlowNetwork.h"
#include <assert.h>
#include <cstdlib>

FlowNetwork& createRandNet(FlowNetwork& net, size_t seed, size_t amountOfNodes, float density, size_t maxCapacity);

#endif // RAND_FLOW_NET_H_
