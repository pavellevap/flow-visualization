#ifndef FLOW_NETWORK_H_
#define FLOW_NETWORK_H_

#include <vector>
#include <list>

class Arc {
public:
    Arc(std::size_t from, std::size_t to, std::size_t capacity, int flow = 0):
        from(from), to(to), capacity(capacity), flow(flow) {}

    std::size_t getRemainingCapacity() const;

    std::size_t getFrom() const;
    std::size_t getTo() const;
    std::size_t getCapacity() const;

	int getFlow() const;
    void addFlow(int flow);
    void setFlow(int flow);

private:
    std::size_t from, to;
    std::size_t capacity;
	int flow;
};

typedef std::list<Arc>::iterator ArcPtr;
typedef std::vector< std::list< ArcPtr > > AdjacencyList;

/**
Транспортная сеть.
Сеть не хранит дуги с нулевой пропускной способностью и петли.
*/
class FlowNetwork {
public:
    FlowNetwork() : source(UNDEFINED_VALUE), sink(UNDEFINED_VALUE) {}

	/// Обнуляет поток
	void resetFlow();

	/// Удаляет все ребра и вершины
	void clear();

	/// Удаляет все ребра и изменяет количество вершин
    void setAmountOfNodes(std::size_t amountOfNodes);

	/// Добавляет дугу с заданной пропускной способностью и потоком
	/// и обратную дугу с нулевой пропускной способностью и отрицательным потоком
    void addArc(std::size_t from, std::size_t to, std::size_t capacity, int flow);
    void addArc(Arc arc);

    void removeArc(ArcPtr arcPtr);

	void addFlow(ArcPtr arcPtr, int flow);

    void setSource(std::size_t sourceNode);
    void setSink(std::size_t sinkNode);
    std::size_t getSource() const;
    std::size_t getSink() const;

    std::size_t getAmountOfNodes() const;

	/// Возвращает величину потока
	unsigned long long getFlow() const;

	/// Возвращает список дуг (без обратных)
    std::list<Arc> getArcs() const;

    /// Возвращает обратную дугу
	ArcPtr getBackArc(ArcPtr arcPtr) const;

    const std::list<ArcPtr>& getArcsFromNode(std::size_t nodeIndex) const;
    const std::list<ArcPtr>& operator [] (std::size_t nodeIndex) const;

    static const std::size_t UNDEFINED_VALUE;

private:
    std::size_t source, sink;
	AdjacencyList adjList;
    std::list<Arc> arcList;

    // Копирование запрещено
    FlowNetwork(const FlowNetwork& net) = delete;
    void operator = (const FlowNetwork& net) = delete;

    bool isIndexCorrect(std::size_t index) const;
    bool isSourceSet() const;
    bool isSinkSet() const;
};

#endif // FLOW_NETWORK_H_

#ifndef MAX_FLOW_ALGORITHM_H_
#define MAX_FLOW_ALGORITHM_H_

#include <vector>
#include <list>

typedef unsigned long long excess_t;

class MaxFlowAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    /// Подготовка к выполнению алгоритма.
    /// Обязательно вызывать в начале findMaxFlow().
    virtual void initialize(FlowNetwork& net) {
        net.resetFlow();
    }

    /// Завершение работы алгоритма (освобождение памяти).
    /// Обязательно вызввать в конце findMaxFlow().
    virtual void finalize() {}
};

class EdmondsKarp : public MaxFlowAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class PushRelabelBasedAlgorithm : public MaxFlowAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    virtual void initialize(FlowNetwork& net);
    virtual void finalize();

    bool canPush(ArcPtr arcPtr);
    void push(FlowNetwork& net, ArcPtr arcPtr);
    void relabel(const FlowNetwork& net, std::size_t nodeIndex);

    std::vector<std::size_t> height;
    std::vector<excess_t> excess;
};

class SimplePushRelabel : public PushRelabelBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class DischargeBasedAlgorithm : public PushRelabelBasedAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    virtual void initialize(FlowNetwork& net);
    virtual void finalize();

    void discharge(FlowNetwork& net, std::size_t nodeIndex);

private:
    /// Для каждого узла хранит указатель на указатель на текущее ребро списка смежности
    std::vector< std::list<ArcPtr>::const_iterator > currentArcPtr;
};

class RelabelToFront :  public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);

private:
    void initialize(FlowNetwork& net);
    void finalize();

    void moveToFront(std::list<std::size_t>::iterator& currentNodePtr);

    std::list<std::size_t> nodes;
};

class DischargeEverything : public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class HighestNodesFirst : public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);

private:
    void initialize(FlowNetwork& net);
    void finalize();

    std::vector< std::list<std::size_t> > nodesAtHeight;
};

#endif // MAX_FLOW_ALGORITHM_H_
#include <iostream>
#include <limits.h>

using std::cerr;
using std::size_t;
using std::list;

const size_t FlowNetwork::UNDEFINED_VALUE = UINT_MAX;

void Arc::addFlow(int flow) {
    if (this->flow + flow > (int)capacity) {
		while (true) {
			flow += 10;
		};
		cerr << flow;
        cerr << "Дуга переполнилась\n";
        return;
    }
    this->flow += flow;
}

void Arc::setFlow(int flow) {
    if (flow > (int)capacity) {
		while (true) {
			flow += 10;
		};
		cerr << flow;
        cerr << "Дуга переполнилось\n";
        return;
    }
    this->flow = flow;
}

int Arc::getFlow() const {
    return flow;
}

size_t Arc::getFrom() const {
    return from;
}

size_t Arc::getTo() const {
    return to;
}

size_t Arc::getCapacity() const {
    return capacity;
}

size_t Arc::getRemainingCapacity() const {
    return capacity - flow;
}

/*FlowNetwork::FlowNetwork(const FlowNetwork& net) {
    (*this) = net;
}

const FlowNetwork& FlowNetwork::operator = (const FlowNetwork& net) {
    clear();
    resize(net.getAmountOfNodes());
    setSink(net.getSink());
    setSource(net.getSource());

    list<Arc> arcs = net.getArcs();
    for (Arc arc : arcs)
        addArc(arc);

    return net;
}*/

void FlowNetwork::resetFlow() {
	for (list<Arc>::iterator it = arcList.begin(); it != arcList.end(); it++)
		(*it).setFlow(0);
}

void FlowNetwork::clear() {
	arcList.clear();
	adjList.clear();
    source = sink = UNDEFINED_VALUE;
}

void FlowNetwork::setAmountOfNodes(size_t amountOfNodes) {
	arcList.clear();
	adjList.clear();
    adjList.resize(amountOfNodes);
    source = sink = UNDEFINED_VALUE;
}

bool FlowNetwork::isIndexCorrect(size_t index) const {
	if (index >= adjList.size()) {
		cerr << "Индекс вершины слишком большой\n";
		int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
		return false;
	}
	return true;
}

bool FlowNetwork::isSourceSet() const {
    if (source == UNDEFINED_VALUE) {
		cerr << "Исток не задан\n";
		int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
		return false;
	}
	return true;
}

bool FlowNetwork::isSinkSet() const {
    if (source == UNDEFINED_VALUE) {
		cerr << "Сток не задан\n";
		int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
		return false;
	}
	return true;
}

void FlowNetwork::addArc(size_t from, size_t to, size_t capacity, int flow) {
	if (!isIndexCorrect(from) || !isIndexCorrect(to))
		return;
	if (flow > (int)capacity) {
		cerr << "Поток больше пропускной способности\n";
		while (true) {
			flow += 10;
		};
		cerr << flow;
		return;
	}

	//if (capacity == 0)
    //    return;
    //if (from == to)
    //    return;

	arcList.push_back(Arc(from, to, capacity, flow));
	adjList[from].push_back(--arcList.end());

	arcList.push_back(Arc(to, from, 0, -flow));
	adjList[to].push_back(--arcList.end());
}

void FlowNetwork::addArc(Arc arc) {
    addArc(arc.getFrom(), arc.getTo(), arc.getCapacity(), arc.getFlow());
}

void FlowNetwork::addFlow(ArcPtr arcPtr, int flow) {
    arcPtr->addFlow(flow);
    getBackArc(arcPtr)->addFlow(-flow);
}

void FlowNetwork::setSource(size_t sourceNode) {
	if (!isIndexCorrect(sourceNode))
		return;
    if (sourceNode == sink) {
        cerr << "Исток и сток совпадают\n";
        int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
        return;
    }

	source = sourceNode;
}

void FlowNetwork::setSink(size_t sinkNode) {
	if (!isIndexCorrect(sinkNode))
		return;
    if (source == sinkNode) {
        cerr << "Исток и сток совпадают\n";
        int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
        return;
    }

	sink = sinkNode;
}

size_t FlowNetwork::getSource() const {
	isSourceSet();
	return source;
}

size_t FlowNetwork::getSink() const {
	isSinkSet();
	return sink;
}

size_t FlowNetwork::getAmountOfNodes() const {
    return adjList.size();
}

unsigned long long FlowNetwork::getFlow() const {
	if (!isSourceSet())
		return 0;

	unsigned long long flow = 0;
	for (list<ArcPtr>::const_iterator it = adjList[source].begin(); it != adjList[source].end(); it++)
		flow += (*it)->getFlow();
	return flow;
}

list<Arc> FlowNetwork::getArcs() const {
	list<Arc> Arcs;
	for (list<Arc>::const_iterator it = arcList.begin(); it != arcList.end(); it++)
        if (it->getCapacity() != 0)
            Arcs.push_back(*it);

	return Arcs;
}

ArcPtr FlowNetwork::getBackArc(ArcPtr arcPtr) const {
    if (arcPtr->getCapacity() == 0)
        return --arcPtr;
    else
        return ++arcPtr;
}

const list<ArcPtr>& FlowNetwork::getArcsFromNode(size_t nodeIndex) const {
    if (!isIndexCorrect(nodeIndex))
        return adjList[0];
    return adjList[nodeIndex];
}

const list<ArcPtr>& FlowNetwork::operator [](size_t nodeIndex) const {
    return getArcsFromNode(nodeIndex);
}

#include <limits.h>
#include <iostream>

using std::vector;
using std::list;
using std::size_t;
using std::cerr;
using std::min;

struct BFSInfo {
    vector<size_t> dist;
    vector<ArcPtr> prevArc;

    size_t getDistToNode(size_t nodeIndex) const;
    bool isNodeReached(size_t nodeIndex) const;
    vector<ArcPtr> getPathToNode(size_t nodeIndex) const;
};

// Запускает BFS из источника в остаточной сети
BFSInfo BFS(const FlowNetwork& net);

bool BFSInfo::isNodeReached(size_t nodeIndex) const {
    return dist[nodeIndex] != FlowNetwork::UNDEFINED_VALUE;
}

size_t BFSInfo::getDistToNode(size_t nodeIndex) const {
    return dist[nodeIndex];
}

vector<ArcPtr> BFSInfo::getPathToNode(size_t nodeIndex) const {
    vector<ArcPtr> path;

    if (!isNodeReached(nodeIndex)) {
        cerr << "Узел недостижим\n";
        int flow = 10;
		while (true) {
			flow += 10;
		};
		cerr << flow;
        return path;
    }

    while (dist[nodeIndex] != 0) {
        path.push_back(prevArc[nodeIndex]);
        nodeIndex = prevArc[nodeIndex]->getFrom();
    }

    return path;
}

BFSInfo BFS(const FlowNetwork& net) {
    BFSInfo info;

    size_t source = net.getSource();
    size_t size = net.getAmountOfNodes();

    info.dist = vector<size_t>(size, FlowNetwork::UNDEFINED_VALUE);
    info.dist[source] = 0;
    info.prevArc.resize(size);
    vector<size_t> layer(1, source);

	while (layer.size() != 0) {
		vector<size_t> newLayer;

		for (int i = 0; i < layer.size(); i++)
			for (list<ArcPtr>::const_iterator it = net[layer[i]].begin(); it != net[layer[i]].end(); it++) {
                size_t from = (*it)->getFrom();
                size_t to = (*it)->getTo();
				if (!info.isNodeReached(to) && (*it)->getRemainingCapacity() > 0) {
					info.dist[to] = info.dist[from] + 1;
					newLayer.push_back(to);
					info.prevArc[to] = (*it);
				}
            }
		layer = newLayer;
	}

	return info;
}

FlowNetwork& EdmondsKarp::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    while (true) {
        BFSInfo info = BFS(net);
        if (!info.isNodeReached(net.getSink()))
            return net;
		vector<ArcPtr> path = info.getPathToNode(net.getSink());

        size_t minCapacity = UINT_MAX;
		for (std::vector<ArcPtr>::const_iterator it = path.begin(); it != path.end(); it++)
			minCapacity = min(minCapacity, (*it)->getRemainingCapacity());

		for (std::vector<ArcPtr>::const_iterator it = path.begin(); it != path.end(); it++)
			net.addFlow((*it), minCapacity);
    }

    finalize();
    return net;
}

void PushRelabelBasedAlgorithm::initialize(FlowNetwork& net) {
    MaxFlowAlgorithm::initialize(net);

    height = vector<size_t>(net.getAmountOfNodes(), 0);
    height[net.getSource()] = net.getAmountOfNodes();

    excess = vector<excess_t>(net.getAmountOfNodes(), 0);
    for (list<ArcPtr>::const_iterator it = net[net.getSource()].begin(); it != net[net.getSource()].end(); it++) {
        excess[(*it)->getTo()] += (*it)->getCapacity();
        net.addFlow((*it), (*it)->getCapacity());
    }
}

void PushRelabelBasedAlgorithm::finalize() {
    MaxFlowAlgorithm::finalize();

    height.clear();
    excess.clear();
}

bool PushRelabelBasedAlgorithm::canPush(ArcPtr arcPtr) {
    size_t from = arcPtr->getFrom();
    size_t to = arcPtr->getTo();
    return arcPtr->getRemainingCapacity() > 0 && height[from] == height[to] + 1;
}

void PushRelabelBasedAlgorithm::push(FlowNetwork& net, ArcPtr arcPtr) {
    size_t flow = min((excess_t)arcPtr->getRemainingCapacity(), excess[arcPtr->getFrom()]);
    excess[arcPtr->getFrom()] -= flow;
    excess[arcPtr->getTo()] += flow;
    net.addFlow(arcPtr, flow);
}

void PushRelabelBasedAlgorithm::relabel(const FlowNetwork& net, size_t nodeIndex) {
    size_t minHeight = UINT_MAX;
    for (list<ArcPtr>::const_iterator it = net[nodeIndex].begin(); it != net[nodeIndex].end(); it++) 
        if ((*it)->getRemainingCapacity() > 0)
            minHeight = min(minHeight, height[(*it)->getTo()]);
    height[nodeIndex] = minHeight + 1;
}

FlowNetwork& SimplePushRelabel::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    bool souldContinue = true;
    while (souldContinue) {
        souldContinue = false;

        for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
            if (i == net.getSink() || i == net.getSource() || excess[i] == 0)
                continue;

            for (list<ArcPtr>::const_iterator it = net[i].begin(); it != net[i].end(); it++) 
                if (canPush(*it) && excess[i] > 0) {
                    push(net, *it);
                    souldContinue = true;
                }

            if (excess[i] > 0) {
                relabel(net, i);
                souldContinue = true;
            }
        }
    }

    finalize();
    return net;
}

void DischargeBasedAlgorithm::initialize(FlowNetwork& net) {
    PushRelabelBasedAlgorithm::initialize(net);

    currentArcPtr.resize(net.getAmountOfNodes());
    for (size_t i = 0; i < net.getAmountOfNodes(); i++)
        currentArcPtr[i] = net[i].begin();
}

void DischargeBasedAlgorithm::finalize() {
    PushRelabelBasedAlgorithm::finalize();

    currentArcPtr.clear();
}

void DischargeBasedAlgorithm::discharge(FlowNetwork& net, size_t nodeIndex) {
    while (excess[nodeIndex] > 0) {
        if (canPush(*currentArcPtr[nodeIndex]))
            push(net, *currentArcPtr[nodeIndex]);

        if (excess[nodeIndex] > 0)
            currentArcPtr[nodeIndex]++;
        if (currentArcPtr[nodeIndex] == net[nodeIndex].end()) {
            relabel(net, nodeIndex);
            currentArcPtr[nodeIndex] = net[nodeIndex].begin();
        }
    }
}

void RelabelToFront::initialize(FlowNetwork &net) {
    DischargeBasedAlgorithm::initialize(net);

    for (size_t i = 0; i < net.getAmountOfNodes(); i++)
        if (i != net.getSink() && i != net.getSource())
            nodes.push_back(i);
}

void RelabelToFront::finalize() {
    DischargeBasedAlgorithm::finalize();

    nodes.clear();
}

void RelabelToFront::moveToFront(list<size_t>::iterator& currentNodePtr) {
    size_t currentNode = *currentNodePtr;
    nodes.erase(currentNodePtr);
    nodes.push_front(currentNode);
    currentNodePtr = nodes.begin();
}

FlowNetwork& RelabelToFront::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    list<size_t>::iterator currentNodePtr = nodes.begin();
    while (currentNodePtr != nodes.end()) {
        size_t oldHeight = height[*currentNodePtr];
        discharge(net, *currentNodePtr);
        if (oldHeight != height[*currentNodePtr])
            moveToFront(currentNodePtr);
        currentNodePtr++;
    }

    finalize();
    return net;
}

FlowNetwork& DischargeEverything::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    bool shouldContinue = true;
    while (shouldContinue) {
        shouldContinue = false;

        for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
            if (i == net.getSource() || i == net.getSink() || excess[i] == 0)
                continue;
            discharge(net, i);
            shouldContinue = true;
        }
    }

    finalize();
    return net;
}

void HighestNodesFirst::initialize(FlowNetwork &net) {
    DischargeBasedAlgorithm::initialize(net);

    nodesAtHeight.resize(net.getAmountOfNodes() * 2);
    for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
        if (i == net.getSink() || i == net.getSource())
            continue;
        nodesAtHeight[height[i]].push_back(i);
    }
}

void HighestNodesFirst::finalize() {
    DischargeBasedAlgorithm::finalize();

    nodesAtHeight.clear();
}

FlowNetwork& HighestNodesFirst::findMaxFlow(FlowNetwork &net) {
    initialize(net);

    size_t currentHeight = 0;
    while (currentHeight != (size_t)-1) {
        list<size_t>& nodes = nodesAtHeight[currentHeight];
        for (list<size_t>::iterator currentNodePtr = nodes.begin(); currentNodePtr != nodes.end(); currentNodePtr++) {
            size_t currentNode = *currentNodePtr;
            if (excess[currentNode] > 0) {
                discharge(net, currentNode);
                if (height[currentNode] > currentHeight) {
                    nodes.erase(currentNodePtr);
                    currentHeight = height[currentNode];
                    nodesAtHeight[currentHeight].push_back(currentNode);
                    break;
                }
            }
        }
        currentHeight--;
    }

    finalize();
    return net;
}


#include <assert.h>
#include <iostream>

using namespace std;

FlowNetwork net;

void readNet() {
    int n, m;
    cin >> n >> m;
    net.setAmountOfNodes(n);
    for (int i = 0; i < m; i++) {
        int from, to, capacity;
        cin >> from >> to >> capacity;
        net.addArc(from - 1, to - 1, capacity, 0);
    }

    /*int s, t;
    cin >> s >> t;
    net.setSource(s);
    net.setSink(t);*/
    net.setSource(0);
    net.setSink(n - 1);
}

void printNet() {
    list<Arc> arcs = net.getArcs();
    for (list<Arc>::iterator it = arcs.begin(); it != arcs.end(); it++)
        cout << (*it).getFrom() << ' ' << (*it).getTo() << ' ' << (*it).getCapacity() << endl;
}

int main() {
	
    readNet();

    MaxFlowAlgorithm* alg1 = new RelabelToFront;
    MaxFlowAlgorithm* alg2 = new DischargeEverything;
    MaxFlowAlgorithm* alg3 = new SimplePushRelabel;
    MaxFlowAlgorithm* alg4 = new HighestNodesFirst;

	
    cout << alg2->findMaxFlow(net).getFlow() << endl;
    
    list<Arc> arcs = net.getArcs();
    for (list<Arc>::iterator it = arcs.begin(); it != arcs.end(); it++)
		cout << (*it).getFlow() << endl;

    return 0;
}
