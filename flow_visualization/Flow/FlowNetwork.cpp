#include <iostream>
#include <limits.h>

#include "FlowNetwork.h"

using std::cerr;
using std::size_t;
using std::list;

const size_t FlowNetwork::UNDEFINED_VALUE = UINT_MAX;

void Arc::addFlow(int flow) {
    if (this->flow + flow > (int)capacity) {
        cerr << "Дуга переполнилась\n";
        return;
    }
    this->flow += flow;
}

void Arc::setFlow(int flow) {
    if (flow > (int)capacity) {
        cerr << "Дуга переполнилось\n";
        return;
    }
    this->flow = flow;
}

int Arc::getFlow() const {
    return flow;
}

size_t Arc::getFrom() const {
    return from;
}

size_t Arc::getTo() const {
    return to;
}

size_t Arc::getCapacity() const {
    return capacity;
}

size_t Arc::getRemainingCapacity() const {
    return capacity - flow;
}

FlowNetwork::FlowNetwork(const FlowNetwork& net) {
    (*this) = net;
}

const FlowNetwork& FlowNetwork::operator = (const FlowNetwork& net) {
    clear();
    setAmountOfNodes(net.getAmountOfNodes());
    setSink(net.getSink());
    setSource(net.getSource());

    list<Arc> arcs = net.getArcs();
    for (Arc arc : arcs)
        addArc(arc);

    return net;
}

void FlowNetwork::resetFlow() {
    for (Arc& arc : arcList)
		arc.setFlow(0);
}

void FlowNetwork::clear() {
	arcList.clear();
	adjList.clear();
    source = sink = UNDEFINED_VALUE;
}

void FlowNetwork::setAmountOfNodes(size_t amountOfNodes) {
	arcList.clear();
	adjList.clear();
    adjList.resize(amountOfNodes);
    source = sink = UNDEFINED_VALUE;
}

bool FlowNetwork::isIndexCorrect(size_t index) const {
	if (index >= adjList.size()) {
		cerr << "Индекс вершины слишком большой\n";
		return false;
	}
	return true;
}

bool FlowNetwork::isSourceSet() const {
    if (source == UNDEFINED_VALUE) {
		cerr << "Исток не задан\n";
		return false;
	}
	return true;
}

bool FlowNetwork::isSinkSet() const {
    if (source == UNDEFINED_VALUE) {
		cerr << "Сток не задан\n";
		return false;
	}
	return true;
}

void FlowNetwork::addArc(size_t from, size_t to, size_t capacity, int flow) {
	if (!isIndexCorrect(from) || !isIndexCorrect(to))
		return;
	if (flow > (int)capacity) {
		cerr << "Поток больше пропускной способности\n";
		return;
	}

	if (capacity == 0)
        return;
    if (from == to)
        return;

	arcList.push_back(Arc(from, to, capacity, flow));
	adjList[from].push_back(--arcList.end());

	arcList.push_back(Arc(to, from, 0, -flow));
	adjList[to].push_back(--arcList.end());
}

void FlowNetwork::addArc(Arc arc) {
    addArc(arc.getFrom(), arc.getTo(), arc.getCapacity(), arc.getFlow());
}

void FlowNetwork::addFlow(ArcPtr arcPtr, int flow) {
    arcPtr->addFlow(flow);
    getOppositeArc(arcPtr)->addFlow(-flow);
}

void FlowNetwork::setSource(size_t sourceNode) {
	if (!isIndexCorrect(sourceNode))
		return;
    if (sourceNode == sink) {
        cerr << "Исток и сток совпадают\n";
        return;
    }

	source = sourceNode;
}

void FlowNetwork::setSink(size_t sinkNode) {
	if (!isIndexCorrect(sinkNode))
		return;
    if (source == sinkNode) {
        cerr << "Исток и сток совпадают\n";
        return;
    }

	sink = sinkNode;
}

size_t FlowNetwork::getSource() const {
	isSourceSet();
	return source;
}

size_t FlowNetwork::getSink() const {
	isSinkSet();
	return sink;
}

size_t FlowNetwork::getAmountOfNodes() const {
    return adjList.size();
}

size_t FlowNetwork::getAmountOfArcs() const {
    return arcList.size() / 2;
}

unsigned long long FlowNetwork::getFlow() const {
	if (!isSourceSet())
		return 0;

	unsigned long long flow = 0;
	for (ArcPtr arcPtr : adjList[source])
		flow += arcPtr->getFlow();
	return flow;
}

list<Arc> FlowNetwork::getArcs() const {
	list<Arc> Arcs;
	for (Arc arc : arcList)
        if (arc.getCapacity() != 0)
            Arcs.push_back(arc);

	return Arcs;
}

ArcPtr FlowNetwork::getOppositeArc(ArcPtr arcPtr) const {
    if (arcPtr->getCapacity() == 0)
        return --arcPtr;
    else
        return ++arcPtr;
}

ArcPtr FlowNetwork::getStraightArc(ArcPtr arcPtr) const {
    if (arcPtr->getCapacity() == 0)
        return --arcPtr;
    return arcPtr;
}

ArcPtr FlowNetwork::getBackArc(ArcPtr arcPtr) const {
    if (arcPtr->getCapacity() > 0)
        return ++arcPtr;
    return arcPtr;
}

const list<ArcPtr>& FlowNetwork::getArcsFromNode(size_t nodeIndex) const {
    if (!isIndexCorrect(nodeIndex))
        return adjList[0];
    return adjList[nodeIndex];
}

const list<ArcPtr>& FlowNetwork::operator [](size_t nodeIndex) const {
    return getArcsFromNode(nodeIndex);
}


