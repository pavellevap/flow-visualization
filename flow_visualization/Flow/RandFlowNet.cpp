#include "RandFlowNet.h"
#include <algorithm>

using std::vector;

FlowNetwork& createRandNet(FlowNetwork& net, size_t seed, size_t amountOfNodes, float density, size_t maxCapacity = 1000000000) {
    assert(density >= 0);
    assert(density <= 1);
    assert(amountOfNodes >= 2);

    srand(seed);

    net.clear();
    vector<Arc> arcs;
    for (size_t i = 0; i < amountOfNodes; i++)
        for (size_t j = 0; j < amountOfNodes; j++) {
            if (i == j) continue;
            size_t capacity = rand() % maxCapacity;
            arcs.push_back(Arc(i, j, capacity, 0));
        }
    random_shuffle(arcs.begin(), arcs.end());

    size_t amountOfArcs = density * amountOfNodes * (amountOfNodes - 1);

    net.setAmountOfNodes(amountOfNodes);
    for (size_t i = 0; i < amountOfArcs; i++) {
        net.addArc(arcs[i]);
    }

    net.setSink(amountOfNodes - 1);
    net.setSource(0);
    
    return net;
}
