#include "MaxFlowAlgorithm.h"
#include <limits.h>
#include <iostream>

using std::vector;
using std::list;
using std::size_t;
using std::cerr;
using std::min;

struct BFSInfo {
    vector<size_t> dist;
    vector<ArcPtr> prevArc;

    size_t getDistToNode(size_t nodeIndex) const;
    bool isNodeReached(size_t nodeIndex) const;
    vector<ArcPtr> getPathToNode(size_t nodeIndex) const;
};

// Запускает BFS из источника в остаточной сети
BFSInfo BFS(const FlowNetwork& net);

bool BFSInfo::isNodeReached(size_t nodeIndex) const {
    return dist[nodeIndex] != FlowNetwork::UNDEFINED_VALUE;
}

size_t BFSInfo::getDistToNode(size_t nodeIndex) const {
    return dist[nodeIndex];
}

vector<ArcPtr> BFSInfo::getPathToNode(size_t nodeIndex) const {
    vector<ArcPtr> path;

    if (!isNodeReached(nodeIndex)) {
        cerr << "Узел недостижим\n";
        return path;
    }

    while (dist[nodeIndex] != 0) {
        path.push_back(prevArc[nodeIndex]);
        nodeIndex = prevArc[nodeIndex]->getFrom();
    }

    return path;
}

BFSInfo BFS(const FlowNetwork& net) {
    BFSInfo info;

    size_t source = net.getSource();
    size_t size = net.getAmountOfNodes();

    info.dist = vector<size_t>(size, FlowNetwork::UNDEFINED_VALUE);
    info.dist[source] = 0;
    info.prevArc.resize(size);
    vector<size_t> layer(1, source);

	while (layer.size() != 0) {
		vector<size_t> newLayer;

		for (size_t node : layer)
			for (ArcPtr arcPtr : net[node]) {
                size_t from = arcPtr->getFrom();
                size_t to = arcPtr->getTo();
				if (!info.isNodeReached(to) && arcPtr->getRemainingCapacity() > 0) {
					info.dist[to] = info.dist[from] + 1;
					newLayer.push_back(to);
					info.prevArc[to] = arcPtr;
				}
            }
		layer = newLayer;
	}

	return info;
}

FlowNetwork& EdmondsKarp::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    while (true) {
        BFSInfo info = BFS(net);
        if (!info.isNodeReached(net.getSink()))
            return net;
		vector<ArcPtr> path = info.getPathToNode(net.getSink());

        size_t minCapacity = UINT_MAX;
		for (ArcPtr arcPtr : path)
			minCapacity = min(minCapacity, arcPtr->getRemainingCapacity());

		for (ArcPtr arcPtr : path)
			net.addFlow(arcPtr, minCapacity);
    }

    finalize();
    return net;
}

void PushRelabelBasedAlgorithm::initialize(FlowNetwork& net) {
    MaxFlowAlgorithm::initialize(net);

    height = vector<size_t>(net.getAmountOfNodes(), 0);
    height[net.getSource()] = net.getAmountOfNodes();

    excess = vector<excess_t>(net.getAmountOfNodes(), 0);
    for (ArcPtr arcPtr : net[net.getSource()]) {
        excess[arcPtr->getTo()] += arcPtr->getCapacity();
        net.addFlow(arcPtr, arcPtr->getCapacity());
    }
}

void PushRelabelBasedAlgorithm::finalize() {
    MaxFlowAlgorithm::finalize();

    height.clear();
    excess.clear();
}

bool PushRelabelBasedAlgorithm::canPush(ArcPtr arcPtr) {
    size_t from = arcPtr->getFrom();
    size_t to = arcPtr->getTo();
    return arcPtr->getRemainingCapacity() > 0 && height[from] == height[to] + 1;
}

void PushRelabelBasedAlgorithm::push(FlowNetwork& net, ArcPtr arcPtr) {
    size_t flow = min((excess_t)arcPtr->getRemainingCapacity(), excess[arcPtr->getFrom()]);
    excess[arcPtr->getFrom()] -= flow;
    excess[arcPtr->getTo()] += flow;
    net.addFlow(arcPtr, flow);
}

void PushRelabelBasedAlgorithm::relabel(const FlowNetwork& net, size_t nodeIndex) {
    size_t minHeight = UINT_MAX;
    for (ArcPtr arcPtr : net[nodeIndex])
        if (arcPtr->getRemainingCapacity() > 0)
            minHeight = min(minHeight, height[arcPtr->getTo()]);
    height[nodeIndex] = minHeight + 1;
}

FlowNetwork& SimplePushRelabel::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    bool souldContinue = true;
    while (souldContinue) {
        souldContinue = false;

        for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
            if (i == net.getSink() || i == net.getSource() || excess[i] == 0)
                continue;

            for (ArcPtr arcPtr : net[i])
                if (canPush(arcPtr) && excess[i] > 0) {
                    push(net, arcPtr);
                    souldContinue = true;
                }

            if (excess[i] > 0) {
                relabel(net, i);
                souldContinue = true;
            }
        }
    }

    finalize();
    return net;
}

void DischargeBasedAlgorithm::initialize(FlowNetwork& net) {
    PushRelabelBasedAlgorithm::initialize(net);

    currentArcPtr.resize(net.getAmountOfNodes());
    for (size_t i = 0; i < net.getAmountOfNodes(); i++)
        currentArcPtr[i] = net[i].begin();
}

void DischargeBasedAlgorithm::finalize() {
    PushRelabelBasedAlgorithm::finalize();

    currentArcPtr.clear();
}

void DischargeBasedAlgorithm::discharge(FlowNetwork& net, size_t nodeIndex) {
    while (excess[nodeIndex] > 0) {
        if (canPush(*currentArcPtr[nodeIndex]))
            push(net, *currentArcPtr[nodeIndex]);

        if (excess[nodeIndex] > 0)
            currentArcPtr[nodeIndex]++;
        if (currentArcPtr[nodeIndex] == net[nodeIndex].end()) {
            relabel(net, nodeIndex);
            currentArcPtr[nodeIndex] = net[nodeIndex].begin();
        }
    }
}

void RelabelToFront::initialize(FlowNetwork &net) {
    DischargeBasedAlgorithm::initialize(net);

    for (size_t i = 0; i < net.getAmountOfNodes(); i++)
        if (i != net.getSink() && i != net.getSource())
            nodes.push_back(i);
}

void RelabelToFront::finalize() {
    DischargeBasedAlgorithm::finalize();

    nodes.clear();
}

void RelabelToFront::moveToFront(list<size_t>::iterator& currentNodePtr) {
    size_t currentNode = *currentNodePtr;
    nodes.erase(currentNodePtr);
    nodes.push_front(currentNode);
    currentNodePtr = nodes.begin();
}

FlowNetwork& RelabelToFront::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    list<size_t>::iterator currentNodePtr = nodes.begin();
    while (currentNodePtr != nodes.end()) {
        size_t oldHeight = height[*currentNodePtr];
        discharge(net, *currentNodePtr);
        if (oldHeight != height[*currentNodePtr])
            moveToFront(currentNodePtr);
        currentNodePtr++;
    }

    finalize();
    return net;
}

FlowNetwork& DischargeEverything::findMaxFlow(FlowNetwork& net) {
    initialize(net);

    bool shouldContinue = true;
    while (shouldContinue) {
        shouldContinue = false;

        for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
            if (i == net.getSource() || i == net.getSink() || excess[i] == 0)
                continue;
            discharge(net, i);
            shouldContinue = true;
        }
    }

    finalize();
    return net;
}

void HighestNodesFirst::initialize(FlowNetwork &net) {
    DischargeBasedAlgorithm::initialize(net);

    nodesAtHeight.resize(net.getAmountOfNodes() * 2);
    for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
        if (i == net.getSink() || i == net.getSource())
            continue;
        nodesAtHeight[height[i]].push_back(i);
    }
}

void HighestNodesFirst::finalize() {
    DischargeBasedAlgorithm::finalize();

    nodesAtHeight.clear();
}

FlowNetwork& HighestNodesFirst::findMaxFlow(FlowNetwork &net) {
    initialize(net);

    size_t currentHeight = 0;
    while (currentHeight != (size_t)-1) {
        list<size_t>& nodes = nodesAtHeight[currentHeight];
        for (auto currentNodePtr = nodes.begin(); currentNodePtr != nodes.end(); currentNodePtr++) {
            size_t currentNode = *currentNodePtr;
            if (excess[currentNode] > 0) {
                discharge(net, currentNode);
                if (height[currentNode] > currentHeight) {
                    nodes.erase(currentNodePtr);
                    currentHeight = height[currentNode];
                    nodesAtHeight[currentHeight].push_back(currentNode);
                    break;
                }
            }
        }
        currentHeight--;
    }

    finalize();
    return net;
}
