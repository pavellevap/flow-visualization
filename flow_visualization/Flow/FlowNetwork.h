#ifndef FLOW_NETWORK_H_
#define FLOW_NETWORK_H_

#include <vector>
#include <list>

class Arc {
public:
    Arc(std::size_t from, std::size_t to, std::size_t capacity, int flow = 0):
        from(from), to(to), capacity(capacity), flow(flow) {}

    std::size_t getRemainingCapacity() const;

    std::size_t getFrom() const;
    std::size_t getTo() const;
    std::size_t getCapacity() const;

	int getFlow() const;
    void addFlow(int flow);
    void setFlow(int flow);

    bool operator == (const Arc arc) const {
        return from == arc.from && to == arc.to && capacity == arc.capacity;
    }

private:
    std::size_t from, to;
    std::size_t capacity;
	int flow;
};

namespace std {
    template<>
    struct hash<Arc> {
        size_t operator () (const Arc arc) const {
            size_t hf = arc.getFrom();
            hf = (hf ^ 0xc70f6907UL) + arc.getTo();
            hf = (hf ^ 0xc70f6907UL) + arc.getCapacity();

            return hf;
        }
    };
}

typedef std::list<Arc>::iterator ArcPtr;
typedef std::vector< std::list< ArcPtr > > AdjacencyList;

/**
Транспортная сеть.
Сеть не хранит дуги с нулевой пропускной способностью и петли.
*/
class FlowNetwork {
public:
    FlowNetwork() : source(UNDEFINED_VALUE), sink(UNDEFINED_VALUE) {}

    // Копирование не желательно, но без него никак.
    FlowNetwork(const FlowNetwork& net);
    const FlowNetwork& operator = (const FlowNetwork& net);

	/// Обнуляет поток
	void resetFlow();
	/// Удаляет все ребра и вершины
	void clear();

	/// Удаляет все ребра и изменяет количество вершин
    void setAmountOfNodes(std::size_t amountOfNodes);
    std::size_t getAmountOfNodes() const;

    /// Возвращает число дуг (не включая обратные)
    std::size_t getAmountOfArcs() const;


	/// Добавляет дугу с заданной пропускной способностью и потоком
	/// и обратную дугу с нулевой пропускной способностью и отрицательным потоком
    void addArc(std::size_t from, std::size_t to, std::size_t capacity, int flow);
    void addArc(Arc arc);

    /// Возвращает противоположную дугу
    ArcPtr getOppositeArc(ArcPtr arcPtr) const;
    /// Возвращает противоположную дугу
    ArcPtr getStraightArc(ArcPtr arcPtr) const;
    /// Возвращает противоположную дугу
    ArcPtr getBackArc(ArcPtr arcPtr) const;


    void removeArc(ArcPtr arcPtr);

    /// Возвращает список дуг (без обратных)
    std::list<Arc> getArcs() const;

    const std::list<ArcPtr>& getArcsFromNode(std::size_t nodeIndex) const;
    const std::list<ArcPtr>& operator [] (std::size_t nodeIndex) const;


	void addFlow(ArcPtr arcPtr, int flow);
    void setFlow(ArcPtr arcPtr, int flow);

    void setSource(std::size_t sourceNode);
    void setSink(std::size_t sinkNode);
    std::size_t getSource() const;
    std::size_t getSink() const;

	/// Возвращает величину потока
	unsigned long long getFlow() const;

    static const std::size_t UNDEFINED_VALUE;

private:
    std::size_t source, sink;
	AdjacencyList adjList;
    std::list<Arc> arcList;

    bool isIndexCorrect(std::size_t index) const;
    bool isSourceSet() const;
    bool isSinkSet() const;
};

#endif // FLOW_NETWORK_H_
