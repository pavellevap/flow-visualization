#ifndef MAX_FLOW_ALGORITHM_H_
#define MAX_FLOW_ALGORITHM_H_

#include <vector>
#include <list>
#include <algorithm>

#include "FlowNetwork.h"
#include "AlgorithmDescription.h"

typedef unsigned long long excess_t;

class MaxFlowAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    /// Подготовка к выполнению алгоритма.
    /// Обязательно вызывать в начале findMaxFlow().
    virtual void initialize(FlowNetwork& net) {
        net.resetFlow();
    }


    /// Завершение работы алгоритма (освобождение памяти).
    /// Обязательно вызввать в конце findMaxFlow().
    virtual void finalize() {}
};

class PushRelabelBasedAlgorithm : public MaxFlowAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    virtual void initialize(FlowNetwork& net);
    virtual void finalize();

    bool canPush(ArcPtr arcPtr);
    void push(FlowNetwork& net, ArcPtr arcPtr);
    void relabel(const FlowNetwork& net, std::size_t nodeIndex);

    std::vector<std::size_t> height;
    std::vector<excess_t> excess;
};

class DischargeBasedAlgorithm : public PushRelabelBasedAlgorithm {
public:
    virtual FlowNetwork& findMaxFlow(FlowNetwork& net) = 0;

protected:
    virtual void initialize(FlowNetwork& net);
    virtual void finalize();

    void discharge(FlowNetwork& net, std::size_t nodeIndex);

private:
    /// Для каждого узла хранит указатель на указатель на текущее ребро списка смежности
    std::vector< std::list<ArcPtr>::const_iterator > currentArcPtr;
};

class EdmondsKarp : public MaxFlowAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class SimplePushRelabel : public PushRelabelBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class RelabelToFront : public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);

private:
    void initialize(FlowNetwork& net);
    void finalize();

    void moveToFront(std::list<std::size_t>::iterator& currentNodePtr);

    std::list<std::size_t> nodes;
};

class DischargeEverything : public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);
};

class HighestNodesFirst : public DischargeBasedAlgorithm {
public:
    FlowNetwork& findMaxFlow(FlowNetwork& net);

private:
    void initialize(FlowNetwork& net);
    void finalize();

    std::vector< std::list<std::size_t> > nodesAtHeight;
};




#endif // MAX_FLOW_ALGORITHM_H_
