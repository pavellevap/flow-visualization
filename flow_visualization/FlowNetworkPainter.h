#ifndef FLOW_NETWORK_PAINTER_H_
#define FLOW_NETWORK_PAINTER_H_

#include "Flow/FlowNetwork.h"
#include "NodeWidget.h"
#include "Flow/AlgorithmDescription.h"

#include <QPainter>
#include <QPoint>
#include <QRect>
#include <QVector2D>
#include <QPolygon>
#include <QString>
#include <vector>
#include <list>
#include <sstream>
#include <string>
#include <unordered_map>

enum Color { USUAL, HIGHLIGHTED };

class FlowNetworkPainter : public QObject {
    Q_OBJECT

public:
    FlowNetworkPainter(QWidget *parent, const FlowNetwork& net,
                       const std::unordered_map<Arc, size_t>& arcColors, const std::vector<size_t>& nodeColors);
    ~FlowNetworkPainter();

    void updateFlowNetwork(const FlowNetwork& net,
                           const std::unordered_map<Arc, size_t>& arcColors,
                           const std::vector<size_t>& nodeColors);

    void show();

    void draw(QPainter *painter);

    static const size_t FRAME_WIDTH;

signals:
    void imageChanged();

private slots:
    void setNodesPositions();

private:
    std::list<Arc> arcs;
    std::unordered_map<Arc, size_t> arcColors;
    std::vector<NodeWidget*> nodeWidgets;
    QRect drawArea;

    void drawArcs(QPainter *painter);
    void drawArc(QPainter *painter, Arc arc);
};

class TextPainter : public QObject {
    Q_OBJECT

public:
    TextPainter(QWidget *parent, const std::string text);

    void show();

    void draw(QPainter *painter);

private:
    QWidget* parent;
    std::string text;
};

class NodeQueuePainter : public QObject {
    Q_OBJECT

public:
    NodeQueuePainter(QWidget* parent,
                     const std::vector<size_t> &nodeColors,
                     const std::vector<std::string> &nodeAttributeNames,
                     const std::vector< std::vector<size_t> > &nodeAttributes);

    void updateNodeQueue(const std::vector<size_t>& nodeColors,
                         std::vector< std::vector<size_t> > nodeAttributes);

    void draw(QPainter *painter);

private:
    QWidget* parent;
    std::vector<size_t> nodeColors;
    std::vector<std::string> nodeAttributeNames;
    std::vector< std::vector<size_t> > nodeAttributes;
};

#endif // FLOW_NETWORK_PAINTER_H_
