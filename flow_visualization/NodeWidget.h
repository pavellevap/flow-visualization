#ifndef NODE_WIDGET_H_
#define NODE_WIDGET_H_

#include <QWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QPoint>
#include <QMouseEvent>
#include <sstream>

class NodeWidget : public QWidget {
    Q_OBJECT

public:
    NodeWidget(QWidget *parent = 0, size_t nodeIndex = 0);

    void setNodeIndex(size_t nodeIndex);
    size_t getNodeIndex() const;

    void setPos(QPoint pos);
    QPoint getPos() const;

    void setPenColor(Qt::GlobalColor color);

    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    static const size_t WIDGET_SIZE;
    static const size_t NODE_SIZE;

signals:
    bool positionChanged();

private:
    size_t nodeIndex;
    QPoint mousePos;

    Qt::GlobalColor penColor;
};

#endif // NODE_WIDGET_H_

