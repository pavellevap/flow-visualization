#include <sstream>

#include "FlowNetworkPainter.h"

using std::vector;
using std::list;
using std::ostringstream;
using std::unordered_map;

const size_t FlowNetworkPainter::FRAME_WIDTH = 40;

FlowNetworkPainter::FlowNetworkPainter(QWidget *parent, const FlowNetwork& net,
                                       const unordered_map<Arc, size_t> &_arcColors,
                                       const vector<size_t> &_nodeColors) {
    drawArea = QRect(FRAME_WIDTH, FRAME_WIDTH,
                     parent->geometry().width() - FRAME_WIDTH * 2,
                     parent->geometry().height() - FRAME_WIDTH * 2);

    nodeWidgets.resize(_nodeColors.size());
    for (size_t i = 0; i < nodeWidgets.size(); i++) {
        nodeWidgets[i] = new NodeWidget(parent, i + 1);
        connect(nodeWidgets[i], SIGNAL(positionChanged()), this, SIGNAL(imageChanged()));
    }
    updateFlowNetwork(net, _arcColors, _nodeColors);
    setNodesPositions();
}

FlowNetworkPainter::~FlowNetworkPainter() {
    for (size_t i = 0; i < nodeWidgets.size(); i++)
        if (nodeWidgets[i] != NULL)
            delete nodeWidgets[i];
}

void FlowNetworkPainter::updateFlowNetwork(const FlowNetwork &net,
                                           const std::unordered_map<Arc, size_t> &_arcColors,
                                           const std::vector<size_t> &_nodeColors) {
    arcs = net.getArcs();
    for (auto it : _arcColors)
        arcColors[it.first] = it.second;
    for (size_t i = 0; i < nodeWidgets.size(); i++) {
        if (i == net.getSource())
            nodeWidgets[i]->setPenColor(Qt::blue);
        else if (i == net.getSink())
            nodeWidgets[i]->setPenColor(Qt::green);
        else if (_nodeColors[i] == Color::USUAL)
            nodeWidgets[i]->setPenColor(Qt::black);

        if (_nodeColors[i] == Color::HIGHLIGHTED)
            nodeWidgets[i]->setPenColor(Qt::red);
    }

}

void FlowNetworkPainter::setNodesPositions() {
    const size_t SPRING_LENGTH = 400;
    const size_t ELECTIC_FORCE = 40000;
    const size_t SPRING_FORCE = 0.9;

    vector< vector<bool> > adjMatrix(nodeWidgets.size(), vector<bool>(nodeWidgets.size(), false));

    for (Arc arc : arcs) {
        adjMatrix[arc.getFrom()][arc.getTo()] = true;
        adjMatrix[arc.getTo()][arc.getFrom()] = true;
    }

    for (size_t i = 0; i < nodeWidgets.size(); i++)
        nodeWidgets[i]->setPos(QPoint(rand() % drawArea.width(), rand() % drawArea.height()));

    const size_t amountOfIterations = 1000;
    for (size_t k = 0; k < amountOfIterations; k++) {
        vector<QVector2D> forces(nodeWidgets.size());
        for (size_t i = 0; i < nodeWidgets.size(); i++) {
            QVector2D force(0, 0);
            for (size_t j = 0; j < nodeWidgets.size(); j++) {
                if (i == j) continue;
                QVector2D dir(nodeWidgets[j]->getPos() - nodeWidgets[i]->getPos());
                if (dir.lengthSquared() < 1)
                    dir = QVector2D(1, 0);
                dir.normalize();

                if (adjMatrix[i][j])
                    force += dir * (QVector2D(nodeWidgets[j]->getPos() - nodeWidgets[i]->getPos()).length() - SPRING_LENGTH) * SPRING_FORCE;
                force -= dir * ELECTIC_FORCE / (QVector2D(nodeWidgets[j]->getPos() - nodeWidgets[i]->getPos()).lengthSquared() + 1);
            }
            forces[i] = force;
        }

        for (size_t i = 0; i < nodeWidgets.size(); i++) {
            QPoint newPos = nodeWidgets[i]->getPos();
            newPos += (forces[i] * (amountOfIterations - k) / amountOfIterations).toPoint();
            if (newPos.x() < drawArea.left())
                newPos.setX(drawArea.left());
            if (newPos.x() > drawArea.right())
                newPos.setX(drawArea.right());
            if (newPos.y() < drawArea.top())
                newPos.setY(drawArea.top());
            if (newPos.y() > drawArea.bottom())
                newPos.setY(drawArea.bottom());
            nodeWidgets[i]->setPos(newPos);
        }
    }

    emit imageChanged();
}

void FlowNetworkPainter::show() {
    for (size_t i = 0; i < nodeWidgets.size(); i++)
        nodeWidgets[i]->show();
}

void FlowNetworkPainter::draw(QPainter *painter) {
    drawArcs(painter);
}

void FlowNetworkPainter::drawArcs(QPainter *painter) {
    for (Arc arc : arcs) {
        drawArc(painter, arc);
    }
}

void FlowNetworkPainter::drawArc(QPainter *painter, Arc arc) {
    QVector2D dir(nodeWidgets[arc.getTo()]->getPos() - nodeWidgets[arc.getFrom()]->getPos());
    QVector2D norm;
    norm.setX(-dir.y());
    norm.setY(dir.x());
    norm.normalize();

    QPoint middle = nodeWidgets[arc.getFrom()]->getPos() + (dir / 2 + norm * 20).toPoint();

    QPainterPath path(nodeWidgets[arc.getFrom()]->getPos());
    path.quadTo(middle, nodeWidgets[arc.getTo()]->getPos());

    ostringstream out;
    out << arc.getFlow() << '/' << arc.getCapacity();
    path.addText(middle, QFont(), out.str().c_str());

    QPolygon arrow;
    arrow << QPoint(25, -2);
    arrow << QPoint(10, 0);
    arrow << QPoint(25, 2);

    if (arcColors[arc] == Color::USUAL)
        painter->setPen(QPen(Qt::black, 1));
    else if (arcColors[arc] == Color::HIGHLIGHTED)
        painter->setPen(QPen(Qt::red, 2));
    painter->drawPath(path);

    painter->save();

    QVector2D vec(middle - nodeWidgets[arc.getTo()]->getPos());
    qreal angle = atan2(vec.y(), vec.x()) / 3.1415 * 180;

    painter->translate(nodeWidgets[arc.getTo()]->getPos());
    painter->rotate(angle);

    if (arcColors[arc] == Color::USUAL)
        painter->setBrush(QBrush(Qt::black, Qt::SolidPattern));
    else if (arcColors[arc] == Color::HIGHLIGHTED)
        painter->setBrush(QBrush(Qt::red, Qt::SolidPattern));
    painter->drawConvexPolygon(arrow);

    painter->restore();

    painter->setPen(QPen());
    painter->setBrush(QBrush());
}

TextPainter::TextPainter(QWidget *parent, const std::string text) {
    this->text = text;
    this->parent = parent;
}

void TextPainter::draw(QPainter *painter) {
    painter->drawText(parent->geometry(), text.c_str());
}


NodeQueuePainter::NodeQueuePainter(QWidget* _parent,
                                   const std::vector<size_t> &_nodeColors,
                                   const std::vector<std::string> &_nodeAttributeNames,
                                   const std::vector<std::vector<size_t> > &_nodeAttributes) {
    parent = _parent;
    nodeAttributeNames = _nodeAttributeNames;
    updateNodeQueue(_nodeColors, _nodeAttributes);
}

void NodeQueuePainter::updateNodeQueue(const std::vector<size_t> &_nodeColors,
                                       std::vector<std::vector<size_t> > _nodeAttributes) {
    nodeColors = _nodeColors;
    nodeAttributes = _nodeAttributes;
}

void NodeQueuePainter::draw(QPainter *painter) {
    size_t x = parent->x();
    size_t y = parent->y();

    for (size_t i = 0; i < nodeColors.size(); i++) {
        if (nodeColors[i] == Color::HIGHLIGHTED)
            painter->setPen(QPen(Qt::red, 2));
        else
            painter->setPen(QPen(Qt::black, 2));

        painter->setBrush(QBrush(Qt::black, Qt::SolidPattern));
        painter->drawEllipse(QPoint(x + 100 + 15 + 30 * i, y + 15), 10, 10);
        painter->setBrush(QBrush());

        painter->setPen(QPen(Qt::white));
        ostringstream out;
        out << i + 1;
        painter->drawText(QRect(x + 100 + 30 * i, y, 30, 30), out.str().c_str(), QTextOption(Qt::AlignCenter));

        painter->setPen(QPen());
    }

    for (size_t i = 0; i < nodeAttributeNames.size(); i++) {
        painter->drawText(QRect(x, y + 30 * (i + 1), 100, 30), nodeAttributeNames[i].c_str(), QTextOption(Qt::AlignCenter));
        for (size_t j = 0; j < nodeAttributes[i].size(); j++) {
            ostringstream out;
            out << nodeAttributes[i][j];
            painter->drawText(QRect(x + 100 + j * 30, y + 30 * (i + 1), 30, 30), out.str().c_str(), QTextOption(Qt::AlignCenter));
        }
    }


}
