#include "AlgorithmVisualizer.h"

using std::min;
using std::vector;
using std::string;

DischargeEverythingVisualizer::DischargeEverythingVisualizer(QWidget* parent, const FlowNetwork &flowNet) {
    parentWidget = parent;
    net = flowNet;
    initializeDescription();
    makeDescription();
    initializeDescription();

    QRect rect = parent->geometry();
    QRect mainRect       (0, 0,                                            rect.width(), rect.height() - 200);
    QRect descriptionRect(0, mainRect.height(),                            rect.width(), 25);
    QRect queueRect      (0, mainRect.height() + descriptionRect.height(), rect.width(), 175);

    mainWidget = new QWidget(parent);
    mainWidget->setGeometry(mainRect);
    mainWidget->show();
    descriptionWidget = new QWidget(parent);
    descriptionWidget->setGeometry(descriptionRect);
    descriptionWidget->show();
    queueWidget = new QWidget(parent);
    queueWidget->setGeometry(queueRect);
    queueWidget->show();

    netPainter = new FlowNetworkPainter(mainWidget, net, arcColors, nodeColors);
    connect(netPainter, SIGNAL(imageChanged()), this, SIGNAL(imageChanged()));
    netPainter->show();

    textPainter = new TextPainter(descriptionWidget, getLastStepDescription());

    vector<string> attributeNames = {"Excess", "Height"};
    vector< vector<size_t> > attributes(2);
    for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
        attributes[0].push_back(nodeExcesses[i]);
        attributes[1].push_back(nodeHeights[i]);
    }
    queuePainter = new NodeQueuePainter(queueWidget, nodeColors, attributeNames, attributes);
}

DischargeEverythingVisualizer::~DischargeEverythingVisualizer() {
    if (netPainter != NULL) {
        delete netPainter;
        netPainter = NULL;
    }
    if (textPainter != NULL) {
        delete textPainter;
        textPainter = NULL;
    }
    if (queuePainter != NULL) {
        delete queuePainter;
        queuePainter = NULL;
    }
    if (mainWidget != NULL) {
        delete mainWidget;
        mainWidget = NULL;
    }
    if (descriptionWidget != NULL) {
        delete descriptionWidget;
        descriptionWidget = NULL;
    }
    if (queueWidget != NULL) {
        delete queueWidget;
        queueWidget = NULL;
    }
}

void DischargeEverythingVisualizer::initializeDescription() {
    net.resetFlow();
    currentStepIndex = 0;
    nodeExcesses.resize(net.getAmountOfNodes());
    nodeHeights.resize(net.getAmountOfNodes());
    nodeColors.resize(net.getAmountOfNodes());
    currentArcPtr.resize(net.getAmountOfNodes());

    for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
        nodeExcesses[i] = 0;
        nodeHeights[i] = 0;
        nodeColors[i] = Color::USUAL;
    }

    for (Arc arc : net.getArcs())
        arcColors[arc] = Color::USUAL;

    nodeHeights[net.getSource()] = net.getAmountOfNodes();
    for (ArcPtr arcPtr : net[net.getSource()]) {
        nodeExcesses[arcPtr->getTo()] += arcPtr->getCapacity();
        net.addFlow(arcPtr, arcPtr->getCapacity());
    }

    for (size_t i = 0; i < net.getAmountOfNodes(); i++)
        currentArcPtr[i] = net[i].begin();
}

void DischargeEverythingVisualizer::makeDescription() {
    Step step;
    addDescription(step, "Запускаем алгоритм \"Simple for\".");
    addStep(step);

    bool shouldContinue = true;
    while (shouldContinue) {
        shouldContinue = false;

        for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
            setNodeColor(step, i, Color::HIGHLIGHTED);
            if (canDischarge(step, i))
                discharge(step, i);
            setNodeColor(step, i, Color::USUAL);
        }

        for (size_t i = 0; i < net.getAmountOfNodes(); i++)
            if (i != net.getSource() && i != net.getSink() && nodeExcesses[i] > 0)
                shouldContinue = true;

        if (shouldContinue) {
            addDescription(step, "Остались еще переполненные вершины. Продолжаем алгоритм");
            addStep(step);
        }
    }

    addDescription(step, "Переполненных вершин не осталось. Завершаем алгоритм \"Simple for\"");
    addStep(step);

    net.resetFlow();
}

void DischargeEverythingVisualizer::addStep(Step &step) {
    steps.push_back(step);
    step = Step();
}

void DischargeEverythingVisualizer::addDescription(Step &step, std::string description) {
    step.description = description;
}

void DischargeEverythingVisualizer::setNodeColor(Step &step, size_t nodeIndex, size_t color) {
    step.actions.push_back(new SetVariableAction<size_t>(&nodeColors[nodeIndex], color));
    nodeColors[nodeIndex] = color;
}

void DischargeEverythingVisualizer::setArcColor(Step &step, Arc arc, size_t color) {
    step.actions.push_back(new SetVariableAction<size_t>(&arcColors[arc], color));
    arcColors[arc] = color;
}

void DischargeEverythingVisualizer::addFlow(Step &step, ArcPtr arcPtr, int flow) {
    step.actions.push_back(new AddFlowAction(arcPtr, flow));
    step.actions.push_back(new AddFlowAction(net.getOppositeArc(arcPtr), -flow));
    net.addFlow(arcPtr, flow);
}

void DischargeEverythingVisualizer::addExcess(Step &step, size_t nodeIndex, int flow) {
    step.actions.push_back(new AddToVariableAction<excess_t>(&nodeExcesses[nodeIndex], flow));
    nodeExcesses[nodeIndex] += flow;
}

void DischargeEverythingVisualizer::setHeight(Step &step, size_t nodeIndex, size_t height) {
    step.actions.push_back(new SetVariableAction<size_t>(&nodeHeights[nodeIndex], height));
    nodeHeights[nodeIndex] = height;
}

bool DischargeEverythingVisualizer::canDischarge(Step &step, size_t nodeIndex) {
    bool can = true;

    if (nodeIndex == net.getSource()) {
        can = false;
        addDescription(step, "Текущая вершина - исток. Discharge запустить нельзя");
    }
    else if (nodeIndex == net.getSink()) {
        can = false;
        addDescription(step, "Текущая вершина - сток. Discharge запустить нельзя");
    }
    else if (nodeExcesses[nodeIndex] == 0) {
        can = false;
        addDescription(step, "Текущая вершина не переполнена. Discharge запустить нельзя");
    }
    else {
        addDescription(step, "К данной вершине применима Discharge");
    }

    addStep(step);

    return can;
}

void DischargeEverythingVisualizer::discharge(Step &step, size_t nodeIndex) {

    addDescription(step, "Запускаем Discharge");
    addStep(step);

    Arc currentArc = *net.getStraightArc(*currentArcPtr[nodeIndex]);
    setArcColor(step, currentArc, Color::HIGHLIGHTED);

    while (nodeExcesses[nodeIndex] > 0) {
        if (canPush(step, *currentArcPtr[nodeIndex]))
            push(step, *currentArcPtr[nodeIndex]);

        if (nodeExcesses[nodeIndex] > 0) {
            setArcColor(step, currentArc, Color::USUAL);
            currentArcPtr[nodeIndex]++;
        }

        if (currentArcPtr[nodeIndex] == net[nodeIndex].end()) {
            addDescription(step, "Все дуги данной вершины просмотрены");
            addStep(step);

            relabel(step, nodeIndex);

            currentArcPtr[nodeIndex] = net[nodeIndex].begin();
            currentArc = *net.getStraightArc(*currentArcPtr[nodeIndex]);
            setArcColor(step, currentArc, Color::HIGHLIGHTED);
        }
        else {
            currentArc = *net.getStraightArc(*currentArcPtr[nodeIndex]);
            setArcColor(step, currentArc, Color::HIGHLIGHTED);
        }
    }
    addDescription(step, "Весь избыток исчерпан. Завершаем Discharge");
    setArcColor(step, currentArc, Color::USUAL);
    addStep(step);
}

bool DischargeEverythingVisualizer::canPush(Step& step, ArcPtr arcPtr) {
    size_t from = arcPtr->getFrom();
    size_t to = arcPtr->getTo();

    if (arcPtr->getRemainingCapacity() > 0 && nodeHeights[from] == nodeHeights[to] + 1) {
        addDescription(step, "К данной дуге применима Push");
        addStep(step);
        return true;
    }
    else {
        addDescription(step, "К данной дуге не применима Push");
        addStep(step);
        return false;
    }
}

void DischargeEverythingVisualizer::push(Step& step, ArcPtr arcPtr) {
    size_t flow = min((excess_t)arcPtr->getRemainingCapacity(), nodeExcesses[arcPtr->getFrom()]);
    addDescription(step, "Push");
    addExcess(step, arcPtr->getFrom(), -flow);
    addExcess(step, arcPtr->getTo(), flow);
    addFlow(step, arcPtr, flow);
    addStep(step);
}

void DischargeEverythingVisualizer::relabel(Step& step, size_t nodeIndex) {
    size_t minHeight = UINT_MAX;
    for (ArcPtr arcPtr : net[nodeIndex])
        if (arcPtr->getRemainingCapacity() > 0)
            minHeight = min(minHeight, nodeHeights[arcPtr->getTo()]);
    addDescription(step, "Relabel");
    setHeight(step, nodeIndex, minHeight + 1);
    addStep(step);
}

void DischargeEverythingVisualizer::draw(QPainter *painter) {
    netPainter->draw(painter);
    textPainter->draw(painter);
    queuePainter->draw(painter);
}

void DischargeEverythingVisualizer::repaint()  {
    netPainter->updateFlowNetwork(net, arcColors, nodeColors);
    netPainter->show();

    if (textPainter != NULL)
        delete textPainter;

    std::ostringstream out;
    out << currentStepIndex << " " << getLastStepDescription() ;
    textPainter = new TextPainter(descriptionWidget, out.str());

    vector< vector<size_t> > attributes(2);
    for (size_t i = 0; i < net.getAmountOfNodes(); i++) {
        attributes[0].push_back(nodeExcesses[i]);
        attributes[1].push_back(nodeHeights[i]);
    }
    queuePainter->updateNodeQueue(nodeColors, attributes);
}
