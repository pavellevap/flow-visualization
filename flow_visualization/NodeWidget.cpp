#include <sstream>

#include "NodeWidget.h"

using std::ostringstream;

const size_t NodeWidget::WIDGET_SIZE = 30;
const size_t NodeWidget::NODE_SIZE = 10;

NodeWidget::NodeWidget(QWidget *parent, size_t nodeIndex) : QWidget(parent) {
    this->nodeIndex = nodeIndex;
    penColor = Qt::black;
    setGeometry(QRect(0, 0, WIDGET_SIZE, WIDGET_SIZE));
}

void NodeWidget::setPos(QPoint pos) {
    move(pos - QPoint(WIDGET_SIZE / 2, WIDGET_SIZE / 2));
}

QPoint NodeWidget::getPos() const {
    return NodeWidget::pos() + QPoint(WIDGET_SIZE / 2, WIDGET_SIZE / 2);
}

void NodeWidget::setPenColor(Qt::GlobalColor color) {
    penColor = color;
}

void NodeWidget::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    ostringstream out;
    out << nodeIndex;

    painter.setPen(QPen(penColor, 3));
    painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
    painter.drawEllipse(QPoint(WIDGET_SIZE / 2, WIDGET_SIZE / 2), NODE_SIZE, NODE_SIZE);
    painter.setBrush(QBrush());
    painter.setPen(QPen());

    painter.setPen(QPen(Qt::white));
    painter.drawText(QRect(0, 0, WIDGET_SIZE, WIDGET_SIZE), out.str().c_str(), QTextOption(Qt::AlignCenter));
    painter.setPen(QPen());

}

void NodeWidget::mousePressEvent(QMouseEvent *event) {
    mousePos = event->globalPos();
}

void NodeWidget::mouseMoveEvent(QMouseEvent *event) {
    QPoint currentMousePos = event->globalPos();

    move(pos() + currentMousePos - mousePos);
    mousePos = currentMousePos;

    emit positionChanged();
}
